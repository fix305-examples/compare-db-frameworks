import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.testing.Test
import org.gradle.kotlin.dsl.dependencies
import org.gradle.kotlin.dsl.withType

class ModulePlugin : Plugin<Project> {
    override fun apply(target: Project) {
        with(target) {
            apply {
                plugin("org.jetbrains.kotlin.jvm")
            }

            tasks.withType<Test>() {
                useJUnitPlatform()
            }

            dependencies {
                "testImplementation"("io.kotest:kotest-runner-junit5:5.9.0")
            }
        }
    }
}