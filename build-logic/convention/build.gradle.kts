plugins {
    `kotlin-dsl`
}

repositories {
    gradlePluginPortal()
}

gradlePlugin {
    plugins {
        register("module") {
            id = "com.fix305.builder.module"
            implementationClass = "ModulePlugin"
        }
    }
}