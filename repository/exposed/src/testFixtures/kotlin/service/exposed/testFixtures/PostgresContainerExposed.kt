package service.exposed.testFixtures

import org.flywaydb.core.Flyway
import org.jetbrains.exposed.sql.Database
import org.postgresql.ds.PGSimpleDataSource
import org.testcontainers.containers.PostgreSQLContainer

object PostgresContainerExposed {
    val db: Database by lazy {
        val container = PostgreSQLContainer<Nothing>("postgres:16")
            .apply { start() }

        val dataSource = PGSimpleDataSource().apply {
            setURL(container.jdbcUrl)
            user = container.username
            password = container.password
        }

        Flyway.configure()
            .locations("db/migration")
            .dataSource(dataSource)
            .load()
            .apply { migrate() }

        Database.connect(dataSource)
    }
}