package service.exposed.dao

import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.javatime.CurrentDateTime
import org.jetbrains.exposed.sql.javatime.datetime

object PersonDocuments : LongIdTable("person_documents") {
    val person = reference("person_id", Persons)
    val type = varchar("type", 32)
    val number = varchar("number", 16)
    val createdAt = datetime("created_at").defaultExpression(CurrentDateTime)
}