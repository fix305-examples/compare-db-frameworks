package service.exposed.dao

import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID

// DAO классы
class PersonDao(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<PersonDao>(Persons)

    var fio by Persons.fio
    var inn by Persons.inn
    var birthdate by Persons.birthdate
    var createdAt by Persons.createdAt

    val documents by PersonDocumentDao referrersOn PersonDocuments.person
}