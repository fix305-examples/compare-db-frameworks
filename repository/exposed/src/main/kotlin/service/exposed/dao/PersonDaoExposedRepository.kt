package service.exposed.dao


import kotlinx.coroutines.Dispatchers
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Op
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.deleteAll
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import repository.contract.PersonRepository
import service.contract.*
import java.time.ZoneOffset

class PersonDaoExposedRepository(
    private val db: Database,
) : PersonRepository {
    override suspend fun create(request: PersonCreate): Person {
        return newSuspendedTransaction(context = Dispatchers.IO, db = db) {
            PersonDao.new {
                this.fio = request.fio
                this.inn = request.inn
                this.birthdate = request.birthdate
            }.toPerson()
        }
    }

    private fun PersonDao.toPerson(): Person {
        return Person(
            id = id.value,
            fio = fio,
            inn = inn,
            birthdate = birthdate,
            createdAt = createdAt.toInstant(ZoneOffset.UTC)
        )
    }

    override suspend fun getById(id: Long): Person? {
        return newSuspendedTransaction(context = Dispatchers.IO, db = db) {
            PersonDao.findById(id)?.toPerson()
        }
    }

    override suspend fun findAll(filter: PersonFilter): List<Person> {
        return newSuspendedTransaction(context = Dispatchers.IO, db = db) {
            val persons = PersonDao.find {
                var condition: Op<Boolean> = Op.TRUE

                filter.fio?.also { fio ->
                    condition = condition and (Persons.fio eq fio)
                }
                filter.inn?.also { inn ->
                    condition = condition and (Persons.inn eq inn)
                }
                filter.birthdate?.also { birthdate ->
                    condition = condition and (Persons.birthdate eq birthdate)
                }

                condition
            }

            // TODO: n+1
            //  В доке указано что есть решение для n+1, но что-то оно сразу так не сработало
            if (filter.passportNumber != null) {
                persons.filter { person ->
                    person.documents.any { document ->
                        document.type == PersonDocument.Type.Passport.name
                                && document.number == filter.passportNumber
                    }
                }
            } else {
                persons
            }.map { it.toPerson() }
        }
    }

    override suspend fun patch(id: Long, request: PersonPatch): Person {
        return newSuspendedTransaction(context = Dispatchers.IO, db = db) {
            val person = PersonDao.findById(id)
                ?: error("Person not found")

            // Обновляются только указанные поля, нет "потерянных данных"
            request.fio?.let { person.fio = it }
            request.inn?.let { person.inn = it }
            request.birthdate?.let { person.birthdate = it }

            person.toPerson()
        }
    }

    override suspend fun addDoc(personId: Long, request: PersonDocumentAdd): PersonDocument {
        return newSuspendedTransaction(context = Dispatchers.IO, db = db) {
            PersonDocumentDao.new {
                this.person = PersonDao[personId]
                this.type = request.type.name
                this.number = request.number
            }.toPersonDocument()
        }
    }

    private fun PersonDocumentDao.toPersonDocument(): PersonDocument {
        return PersonDocument(
            id = id.value,
            type = PersonDocument.Type.entries.single { it.name == type },
            number = number,
            createdAt = createdAt.toInstant(ZoneOffset.UTC)
        )
    }

    override suspend fun getDocs(personId: Long): List<PersonDocument> {
        return newSuspendedTransaction(context = Dispatchers.IO, db = db) {
            PersonDocumentDao.find {
                PersonDocuments.person.eq(personId)
            }.map { it.toPersonDocument() }

            /*
            n+1 вариант
            PersonDao[personId].documents.toList()
                .map { it.toPersonDocument() }
             */
        }
    }

    override suspend fun getPersonWithPassport(passport: String): PersonWithPassport? {
        return newSuspendedTransaction(context = Dispatchers.IO, db = db) {
            // n+1
            PersonDocumentDao.find {
                PersonDocuments.number eq passport and (PersonDocuments.type eq "Passport")
            }.map { doc ->
                PersonWithPassport(
                    fio = doc.person.fio,
                    passportNumber = doc.number
                )
            }.singleOrNull()
        }
    }

    override suspend fun clean() {
        return newSuspendedTransaction(context = Dispatchers.IO, db = db) {
            PersonDocuments.deleteAll()
            Persons.deleteAll()
        }
    }
}
