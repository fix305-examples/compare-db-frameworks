package service.exposed.dao

import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID

class PersonDocumentDao(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<PersonDocumentDao>(PersonDocuments)

    var person by PersonDao referencedOn PersonDocuments.person
    var type by PersonDocuments.type
    var number by PersonDocuments.number
    var createdAt by PersonDocuments.createdAt
}