package service.exposed.dao

import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.javatime.CurrentDateTime
import org.jetbrains.exposed.sql.javatime.date
import org.jetbrains.exposed.sql.javatime.datetime

// Таблицы
object Persons : LongIdTable("person") {
    val fio = varchar("fio", 255)
    val inn = varchar("inn", 12)
    val birthdate = date("birthdate")
    val createdAt = datetime("created_at").defaultExpression(CurrentDateTime)
}