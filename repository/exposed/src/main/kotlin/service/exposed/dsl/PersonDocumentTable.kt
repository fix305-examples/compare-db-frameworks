package service.exposed.dsl

import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.javatime.CurrentTimestamp
import org.jetbrains.exposed.sql.javatime.timestamp

object PersonDocumentTable : Table("person_documents") {
    val id = long("id").autoIncrement()
    val personId = long("person_id").index()
    val type = varchar("type", 32)
    val number = varchar("number", 16)
    val createdAt = timestamp("created_at").defaultExpression(CurrentTimestamp)

    override val primaryKey: PrimaryKey = PrimaryKey(id)
}