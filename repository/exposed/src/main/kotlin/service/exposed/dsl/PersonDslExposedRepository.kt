package service.exposed.dsl

import kotlinx.coroutines.Dispatchers
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import repository.contract.PersonRepository
import service.contract.*

class PersonDslExposedRepository(
    private val db: Database,
) : PersonRepository {
    override suspend fun create(request: PersonCreate): Person {
        return newSuspendedTransaction(context = Dispatchers.IO, db = db) {
            PersonTable.insertReturning {
                it[fio] = request.fio
                it[inn] = request.inn
                it[birthdate] = request.birthdate
            }.single()
                .toPerson()
        }
    }

    private fun ResultRow.toPerson(): Person {
        return Person(
            id = this[PersonTable.id],
            fio = this[PersonTable.fio],
            inn = this[PersonTable.inn],
            birthdate = this[PersonTable.birthdate],
            createdAt = this[PersonTable.createdAt]
        )
    }

    override suspend fun getById(id: Long): Person? {
        return newSuspendedTransaction(context = Dispatchers.IO, db = db) {
            PersonTable.selectAll()
                .where { PersonTable.id eq id }
                .singleOrNull()
                ?.toPerson()
        }
    }

    override suspend fun findAll(filter: PersonFilter): List<Person> {
        return newSuspendedTransaction(context = Dispatchers.IO, db = db) {
            val query = PersonTable.selectAll()

            filter.fio?.also { fio -> query.andWhere { PersonTable.fio eq fio } }
            filter.inn?.also { inn -> query.andWhere { PersonTable.inn eq inn } }
            filter.birthdate?.also { birthdate -> query.andWhere { PersonTable.birthdate eq birthdate } }
            filter.passportNumber?.also { num ->
                query.adjustColumnSet {
                    leftJoin(
                        otherTable = PersonDocumentTable,
                        onColumn = { PersonTable.id },
                        otherColumn = { PersonDocumentTable.personId }
                    )
                }
                query.andWhere { PersonDocumentTable.type eq PersonDocument.Type.Passport.name }
                query.andWhere { PersonDocumentTable.number eq num }
            }

            query.map { it.toPerson() }
        }
    }

    override suspend fun patch(id: Long, request: PersonPatch): Person {
        return newSuspendedTransaction(context = Dispatchers.IO, db = db) {
            PersonTable.updateReturning(where = {
                PersonTable.id eq id
            }) { rec ->
                request.fio?.also { rec[fio] = it }
                request.inn?.also { rec[inn] = it }
                request.birthdate?.also { rec[birthdate] = it }
            }
                .single()
                .toPerson()
        }
    }

    override suspend fun addDoc(personId: Long, request: PersonDocumentAdd): PersonDocument {
        return newSuspendedTransaction(context = Dispatchers.IO, db = db) {
            PersonDocumentTable.insertReturning {
                it[PersonDocumentTable.personId] = personId
                it[PersonDocumentTable.type] = request.type.name
                it[PersonDocumentTable.number] = request.number
            }
                .single()
                .toDocument()
        }
    }

    private fun ResultRow.toDocument(): PersonDocument {
        return PersonDocument(
            id = this[PersonDocumentTable.id],
            type = PersonDocument.Type.entries
                .single { it.name == this[PersonDocumentTable.type] },
            number = this[PersonDocumentTable.number],
            createdAt = this[PersonDocumentTable.createdAt]
        )
    }

    override suspend fun getDocs(personId: Long): List<PersonDocument> {
        return newSuspendedTransaction(context = Dispatchers.IO, db = db) {
            PersonDocumentTable.selectAll()
                .where { PersonDocumentTable.personId eq personId }
                .map { it.toDocument() }
        }
    }

    override suspend fun getPersonWithPassport(passport: String): PersonWithPassport? {
        return newSuspendedTransaction(context = Dispatchers.IO, db = db) {
            PersonDocumentTable
                .innerJoin(
                    otherTable = PersonTable,
                    onColumn = { PersonTable.id },
                    otherColumn = { PersonDocumentTable.personId }
                )
                .selectAll()
                .where { PersonDocumentTable.type eq PersonDocument.Type.Passport.name }
                .andWhere { PersonDocumentTable.number eq passport }
                .singleOrNull()
                ?.toPersonWithPassport()
        }
    }

    private fun ResultRow.toPersonWithPassport(): PersonWithPassport {
        return PersonWithPassport(
            fio = this[PersonTable.fio],
            passportNumber = this[PersonDocumentTable.number]
        )
    }

    override suspend fun clean() {
        return newSuspendedTransaction(context = Dispatchers.IO, db = db) {
            PersonDocumentTable.deleteAll()
            PersonTable.deleteAll()
        }
    }
}