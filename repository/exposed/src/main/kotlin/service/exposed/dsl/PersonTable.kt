package service.exposed.dsl

import org.jetbrains.exposed.sql.Table
import org.jetbrains.exposed.sql.javatime.CurrentTimestamp
import org.jetbrains.exposed.sql.javatime.date
import org.jetbrains.exposed.sql.javatime.timestamp

object PersonTable : Table("person") {
    val id = long("id").autoIncrement()
    val fio = varchar("fio", 255)
    val inn = varchar("inn", 12)
    val birthdate = date("birthdate")
    val createdAt = timestamp("created_at").defaultExpression(CurrentTimestamp)

    override val primaryKey: PrimaryKey = PrimaryKey(id)
}