plugins {
    id("com.fix305.builder.module")
    id("java-test-fixtures")
}

val exposedVersion = "0.50.0"

dependencies {
    api(project(":repository:contract"))

    implementation("org.postgresql:postgresql:42.7.1")

    api("org.jetbrains.exposed", "exposed-core", exposedVersion)
    api("org.jetbrains.exposed", "exposed-dao", exposedVersion)
    api("org.jetbrains.exposed", "exposed-jdbc", exposedVersion)
    api("org.jetbrains.exposed", "exposed-java-time", exposedVersion)

    implementation("ch.qos.logback", "logback-classic", "1.3.5")
    implementation("org.slf4j", "slf4j-api", "2.0.4")

    testFixturesImplementation(project(":flyway"))
    testFixturesImplementation("org.flywaydb:flyway-core:10.11.0")
    testFixturesImplementation("org.flywaydb:flyway-database-postgresql:10.11.0")

    testFixturesImplementation("org.testcontainers:testcontainers:1.19.8")
    testFixturesImplementation("org.testcontainers:postgresql:1.19.8")
}
repositories {
    mavenCentral()
}
