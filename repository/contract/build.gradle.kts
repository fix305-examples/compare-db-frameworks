plugins {
    id("com.fix305.builder.module")
}

dependencies {
    api(project(":service:contract"))
}

repositories {
    mavenCentral()
}
