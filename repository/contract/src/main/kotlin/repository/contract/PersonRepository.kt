package repository.contract

import service.contract.*

interface PersonRepository {
    suspend fun create(request: PersonCreate): Person
    suspend fun getById(id: Long): Person?

    suspend fun findAll(filter: PersonFilter = PersonFilter.Empty): List<Person>
    suspend fun patch(id: Long, request: PersonPatch): Person

    /** Documents */
    suspend fun addDoc(personId: Long, request: PersonDocumentAdd): PersonDocument
    suspend fun getDocs(personId: Long): List<PersonDocument>

    /** Combine request */
    suspend fun getPersonWithPassport(passport: String): PersonWithPassport?

    /** Delete all data */
    suspend fun clean()
}