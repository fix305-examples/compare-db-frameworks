package service.jooq.testFixtures

import org.flywaydb.core.Flyway
import org.jooq.DSLContext
import org.jooq.SQLDialect
import org.jooq.impl.DSL
import org.jooq.impl.DefaultConfiguration
import org.postgresql.ds.PGSimpleDataSource
import org.testcontainers.containers.PostgreSQLContainer

object PostgresContainerJooq {
    val db: DSLContext by lazy {
        val container = PostgreSQLContainer<Nothing>("postgres:14.2")
            .apply { start() }

        val dataSource = PGSimpleDataSource().apply {
            setURL(container.jdbcUrl)
            user = container.username
            password = container.password
        }

        Flyway.configure()
            .locations("db/migration")
            .dataSource(dataSource)
            .load()
            .apply { migrate() }

        DSL.using(
            DefaultConfiguration()
                .set(dataSource)
                .set(SQLDialect.POSTGRES)
        )
    }
}