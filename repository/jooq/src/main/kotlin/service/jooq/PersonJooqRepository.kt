package service.jooq

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jooq.DSLContext
import org.jooq.Record
import org.jooq.UpdateSetMoreStep
import repository.contract.PersonRepository
import service.contract.*
import service.jooq.generated.tables.records.PersonDocumentsRecord
import service.jooq.generated.tables.records.PersonRecord
import service.jooq.generated.tables.references.PERSON
import service.jooq.generated.tables.references.PERSON_DOCUMENTS

class PersonJooqRepository(
    private val db: DSLContext
) : PersonRepository {
    override suspend fun create(request: PersonCreate): Person = withContext(Dispatchers.IO) {
        db.insertInto(PERSON)
            .set(request.toPersonRecord())
            .returning()
            .single()
            .toPerson()
    }

    private fun PersonCreate.toPersonRecord(): PersonRecord {
        return PersonRecord(
            id = null,
            fio = fio,
            inn = inn,
            birthdate = birthdate,
            createdAt = null,
        )
    }

    override suspend fun getById(id: Long): Person? = withContext(Dispatchers.IO) {
        db.selectFrom(PERSON)
            .where(PERSON.ID.eq(id))
            .singleOrNull()
            ?.toPerson()
    }

    private fun PersonRecord.toPerson(): Person {
        return Person(
            id = id ?: error("shouldn't be null"),
            fio = fio,
            inn = inn,
            birthdate = birthdate,
            createdAt = createdAt?.toInstant() ?: error("shouldn't be null")
        )
    }

    override suspend fun findAll(filter: PersonFilter): List<Person> {
        return withContext(Dispatchers.IO) {
            val query = db.select()
                .from(PERSON)

            filter.fio?.let { fio ->
                query.where(PERSON.FIO.eq(fio))
            }
            filter.inn?.let { inn ->
                query.where(PERSON.INN.eq(inn))
            }
            filter.birthdate?.let { birthdate ->
                query.where(PERSON.BIRTHDATE.eq(birthdate))
            }
            filter.passportNumber?.let { num ->
                query.join(PERSON_DOCUMENTS)
                    .on(PERSON.ID.eq(PERSON_DOCUMENTS.PERSON_ID))
                    .where(PERSON_DOCUMENTS.TYPE.eq(PersonDocument.Type.Passport.name))
                    .and(PERSON_DOCUMENTS.NUMBER.eq(num))
            }

            query.fetchInto(PersonRecord::class.java).map { it.toPerson() }
        }
    }

    override suspend fun patch(id: Long, request: PersonPatch): Person {
        val updateQuery = db.update(PERSON) as UpdateSetMoreStep<PersonRecord>

        request.fio?.let { updateQuery.set(PERSON.FIO, it) }
        request.inn?.let { updateQuery.set(PERSON.INN, it) }
        request.birthdate?.let { updateQuery.set(PERSON.BIRTHDATE, it) }

        return updateQuery.where(PERSON.ID.eq(id))
            .returning()
            .single()
            .toPerson()
    }

    override suspend fun addDoc(
        personId: Long,
        request: PersonDocumentAdd,
    ): PersonDocument = withContext(Dispatchers.IO) {
        db.insertInto(PERSON_DOCUMENTS)
            .set(request.toRecord(personId))
            .returning()
            .single()
            .toPersonDocument()
    }

    private fun PersonDocumentAdd.toRecord(personId: Long): PersonDocumentsRecord {
        return PersonDocumentsRecord(
            id = null,
            personId = personId,
            type = type.name,
            number = number,
            createdAt = null,
        )
    }

    private fun PersonDocumentsRecord.toPersonDocument(): PersonDocument {
        return PersonDocument(
            id = id
                ?: error("shouldn't be null"),
            type = PersonDocument.Type.entries.find { it.name == type }
                ?: error("`$type` not found"),
            number = number,
            createdAt = createdAt?.toInstant()
                ?: error("shouldn't be null")
        )
    }

    override suspend fun getDocs(personId: Long): List<PersonDocument> = withContext(Dispatchers.IO) {
        db.selectFrom(PERSON_DOCUMENTS)
            .where(PERSON_DOCUMENTS.PERSON_ID.eq(personId))
            .map { it.toPersonDocument() }
    }

    override suspend fun getPersonWithPassport(passport: String): PersonWithPassport? {
        return withContext(Dispatchers.IO) {
            db.select()
                .from(PERSON_DOCUMENTS)
                .join(PERSON)
                .on(PERSON.ID.eq(PERSON_DOCUMENTS.PERSON_ID))
                .where(PERSON_DOCUMENTS.TYPE.eq(PersonDocument.Type.Passport.name))
                .and(PERSON_DOCUMENTS.NUMBER.eq(passport))
                .singleOrNull()
                ?.toPersonWithPassport()
        }
    }

    private fun Record.toPersonWithPassport(): PersonWithPassport {
        return PersonWithPassport(
            fio = this[PERSON.FIO]!!,
            passportNumber = this[PERSON_DOCUMENTS.NUMBER]!!
        )
    }

    override suspend fun clean() {
        withContext(Dispatchers.IO) {
            db.deleteFrom(PERSON_DOCUMENTS).execute()
            db.deleteFrom(PERSON).execute()
        }
    }
}
