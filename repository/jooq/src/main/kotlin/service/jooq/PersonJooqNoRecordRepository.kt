package service.jooq

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jooq.DSLContext
import org.jooq.Record
import org.jooq.UpdateSetMoreStep
import repository.contract.PersonRepository
import service.contract.*
import service.jooq.generated.tables.references.PERSON
import service.jooq.generated.tables.references.PERSON_DOCUMENTS

class PersonJooqNoRecordRepository(
    private val db: DSLContext
) : PersonRepository {
    override suspend fun create(request: PersonCreate): Person = withContext(Dispatchers.IO) {
        db.insertInto(PERSON)
            .set(PERSON.FIO, request.fio)
            .set(PERSON.INN, request.inn)
            .set(PERSON.BIRTHDATE, request.birthdate)
            .returning()
            .single()
            .toPerson()
    }

    override suspend fun getById(id: Long): Person? = withContext(Dispatchers.IO) {
        db.selectFrom(PERSON)
            .where(PERSON.ID.eq(id))
            .singleOrNull()
            ?.toPerson()
    }

    private fun Record.toPerson(): Person {
        return Person(
            id = this[PERSON.ID]!!,
            fio = this[PERSON.FIO]!!,
            inn = this[PERSON.INN]!!,
            birthdate = this[PERSON.BIRTHDATE]!!,
            createdAt = this[PERSON.CREATED_AT]!!.toInstant()
        )
    }

    override suspend fun findAll(filter: PersonFilter): List<Person> {
        return withContext(Dispatchers.IO) {
            val query = db.select()
                .from(PERSON)

            filter.fio?.let { fio ->
                query.where(PERSON.FIO.eq(fio))
            }
            filter.inn?.let { inn ->
                query.where(PERSON.INN.eq(inn))
            }
            filter.birthdate?.let { birthdate ->
                query.where(PERSON.BIRTHDATE.eq(birthdate))
            }
            filter.passportNumber?.let { num ->
                query.join(PERSON_DOCUMENTS)
                    .on(PERSON.ID.eq(PERSON_DOCUMENTS.PERSON_ID))
                    .where(PERSON_DOCUMENTS.TYPE.eq(PersonDocument.Type.Passport.name))
                    .and(PERSON_DOCUMENTS.NUMBER.eq(num))
            }

            query.map { it.toPerson() }
        }
    }

    override suspend fun patch(id: Long, request: PersonPatch): Person {
        val updateQuery = db.update(PERSON) as UpdateSetMoreStep<Record>

        request.fio?.let { updateQuery.set(PERSON.FIO, it) }
        request.inn?.let { updateQuery.set(PERSON.INN, it) }
        request.birthdate?.let { updateQuery.set(PERSON.BIRTHDATE, it) }

        return updateQuery.where(PERSON.ID.eq(id))
            .returning()
            .single()
            .toPerson()
    }

    override suspend fun addDoc(
        personId: Long,
        request: PersonDocumentAdd,
    ): PersonDocument = withContext(Dispatchers.IO) {
        db.insertInto(PERSON_DOCUMENTS)
            .set(PERSON_DOCUMENTS.PERSON_ID, personId)
            .set(PERSON_DOCUMENTS.TYPE, request.type.name)
            .set(PERSON_DOCUMENTS.NUMBER, request.number)
            .returning()
            .single()
            .toPersonDocument()
    }

    private fun Record.toPersonDocument(): PersonDocument {
        return PersonDocument(
            id = this[PERSON_DOCUMENTS.ID]!!,
            type = PersonDocument.Type.entries.find { it.name == this[PERSON_DOCUMENTS.TYPE]!! }
                ?: error("`${this[PERSON_DOCUMENTS.TYPE]}` not found"),
            number = this[PERSON_DOCUMENTS.NUMBER]!!,
            createdAt = this[PERSON_DOCUMENTS.CREATED_AT]!!.toInstant()
        )
    }

    override suspend fun getDocs(personId: Long): List<PersonDocument> = withContext(Dispatchers.IO) {
        db.selectFrom(PERSON_DOCUMENTS)
            .where(PERSON_DOCUMENTS.PERSON_ID.eq(personId))
            .map { it.toPersonDocument() }
    }

    override suspend fun getPersonWithPassport(passport: String): PersonWithPassport? {
        return withContext(Dispatchers.IO) {
            db.select()
                .from(PERSON_DOCUMENTS)
                .join(PERSON)
                .on(PERSON.ID.eq(PERSON_DOCUMENTS.PERSON_ID))
                .where(PERSON_DOCUMENTS.TYPE.eq(PersonDocument.Type.Passport.name))
                .and(PERSON_DOCUMENTS.NUMBER.eq(passport))
                .singleOrNull()
                ?.toPersonWithPassport()
        }
    }

    private fun Record.toPersonWithPassport(): PersonWithPassport {
        return PersonWithPassport(
            fio = this[PERSON.FIO]!!,
            passportNumber = this[PERSON_DOCUMENTS.NUMBER]!!
        )
    }

    override suspend fun clean() {
        withContext(Dispatchers.IO) {
            db.deleteFrom(PERSON_DOCUMENTS).execute()
            db.deleteFrom(PERSON).execute()
        }
    }
}