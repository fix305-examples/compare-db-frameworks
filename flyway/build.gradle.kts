plugins {
    id("com.fix305.builder.module")

    id("org.flywaydb.flyway") version "9.22.3"
}

flyway {
    driver = "org.postgresql.Driver"
    url = "jdbc:postgresql://127.0.0.1:5432/db_frameworks"
    user = "admin"
    password = "password"
}

dependencies {
    implementation("org.postgresql:postgresql:42.7.1")
}