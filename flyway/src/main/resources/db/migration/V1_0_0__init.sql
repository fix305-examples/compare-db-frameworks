CREATE TABLE IF NOT EXISTS person
(
    id         bigserial PRIMARY KEY,
    fio        varchar(255)              NOT NULL,
    inn        varchar(12)               NOT NULL,
    birthdate  date                      NOT NULL,
    created_at timestamptz DEFAULT NOW() NOT NULL
);

CREATE TABLE IF NOT EXISTS person_documents
(
    id         bigserial PRIMARY KEY,
    person_id  bigint                    NOT NULL references person(id),
    type       varchar(16)               NOT NULL,
    number     varchar(32)               NOT NULL,
    created_at timestamptz DEFAULT NOW() NOT NULL
)