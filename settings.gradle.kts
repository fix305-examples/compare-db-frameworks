rootProject.name = "db-frameworks"

dependencyResolutionManagement {
    repositories {
        mavenCentral()
    }
}

includeBuild("build-logic")

include(
    "flyway",

    "app:spring",

    "service:contract",
    "service:impl",

    "repository:contract",
    "repository:exposed",
    "repository:jooq"
)