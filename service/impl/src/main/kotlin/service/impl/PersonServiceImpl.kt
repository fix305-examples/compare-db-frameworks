package service.impl

import repository.contract.PersonRepository
import service.contract.*

class PersonServiceImpl(
    private val repository: PersonRepository,
) : PersonService {
    override suspend fun create(request: PersonCreate): Person {
        return repository.create(request)
    }

    override suspend fun getById(id: Long): Person? {
        return repository.getById(id)
    }

    override suspend fun findAll(filter: PersonFilter): List<Person> {
        return repository.findAll(filter)
    }

    override suspend fun patch(id: Long, request: PersonPatch): Person {
        return repository.patch(id, request)
    }

    override suspend fun addDoc(personId: Long, request: PersonDocumentAdd): PersonDocument {
        return repository.addDoc(personId, request)
    }

    override suspend fun getDocs(personId: Long): List<PersonDocument> {
        return repository.getDocs(personId)
    }

    override suspend fun getPersonWithPassport(passport: String): PersonWithPassport? {
        return repository.getPersonWithPassport(passport)
    }
}