package service.impl

import io.kotest.core.test.TestCase
import repository.contract.PersonRepository
import service.contract.testFixtures.PersonServiceBaseTest
import service.jooq.PersonJooqRepository
import service.jooq.testFixtures.PostgresContainerJooq

private val repository: PersonRepository = PersonJooqRepository(
    db = PostgresContainerJooq.db
)

class PersonServiceJooqTest : PersonServiceBaseTest({
    PersonServiceImpl(repository = repository)
}) {
    override suspend fun beforeEach(testCase: TestCase) {
        repository.clean()
    }
}

