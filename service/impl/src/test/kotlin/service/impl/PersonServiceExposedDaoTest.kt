package service.impl

import io.kotest.core.test.TestCase
import repository.contract.PersonRepository
import service.contract.testFixtures.PersonServiceBaseTest
import service.exposed.dao.PersonDaoExposedRepository
import service.exposed.testFixtures.PostgresContainerExposed

private val repositoryDao: PersonRepository = PersonDaoExposedRepository(db = PostgresContainerExposed.db)

class PersonServiceExposedDaoTest : PersonServiceBaseTest({
    PersonServiceImpl(repositoryDao)
}) {
    override suspend fun beforeEach(testCase: TestCase) {
        repositoryDao.clean()
    }
}