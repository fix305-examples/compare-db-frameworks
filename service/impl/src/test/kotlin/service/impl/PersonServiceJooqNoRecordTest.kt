package service.impl

import io.kotest.core.test.TestCase
import repository.contract.PersonRepository
import service.contract.testFixtures.PersonServiceBaseTest
import service.jooq.PersonJooqNoRecordRepository
import service.jooq.testFixtures.PostgresContainerJooq

private val repository: PersonRepository = PersonJooqNoRecordRepository(
    db = PostgresContainerJooq.db
)

class PersonServiceJooqNoRecordTest : PersonServiceBaseTest({
    PersonServiceImpl(repository = repository)
}) {
    override suspend fun beforeEach(testCase: TestCase) {
        repository.clean()
    }
}