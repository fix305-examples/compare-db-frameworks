package service.impl

import io.kotest.core.test.TestCase
import repository.contract.PersonRepository
import service.contract.testFixtures.PersonServiceBaseTest
import service.exposed.dsl.PersonDslExposedRepository
import service.exposed.testFixtures.PostgresContainerExposed

private val repositoryDsl: PersonRepository = PersonDslExposedRepository(db = PostgresContainerExposed.db)

class PersonServiceExposedTest : PersonServiceBaseTest({
    PersonServiceImpl(repositoryDsl)
}) {
    override suspend fun beforeEach(testCase: TestCase) {
        repositoryDsl.clean()
    }
}

