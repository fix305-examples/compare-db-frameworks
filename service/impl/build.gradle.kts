plugins {
    id("com.fix305.builder.module")
    id("java-test-fixtures")
}

dependencies {
    api(project(":service:contract"))
    api(project(":repository:contract"))

    testImplementation("io.kotest:kotest-runner-junit5:5.9.0")
    testFixturesImplementation("io.kotest:kotest-runner-junit5:5.9.0")

    implementation("ch.qos.logback", "logback-classic", "1.3.5")
    implementation("org.slf4j", "slf4j-api", "2.0.4")

    testImplementation(testFixtures(project(":service:contract")))
    testImplementation(testFixtures(project(":repository:exposed")))
    testImplementation(testFixtures(project(":repository:jooq")))
}