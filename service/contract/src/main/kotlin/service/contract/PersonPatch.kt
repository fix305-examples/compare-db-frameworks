package service.contract

import java.time.LocalDate

data class PersonPatch(
    val fio: String? = null,
    val inn: String? = null,
    val birthdate: LocalDate? = null,
)