package service.contract

data class PersonWithPassport(
    val fio: String,
    val passportNumber: String
)