package service.contract

data class PersonDocumentAdd(
    val type: PersonDocument.Type,
    val number: String
)