package service.contract

import java.time.Instant
import java.time.LocalDate

data class Person(
    val id: Long,
    val fio: String,
    val inn: String,
    val birthdate: LocalDate,
    val createdAt: Instant
)