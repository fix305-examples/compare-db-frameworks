package service.contract

import java.time.LocalDate

data class PersonCreate(
    val fio: String,
    val inn: String,
    val birthdate: LocalDate,
)