package service.contract

import java.time.LocalDate

data class PersonFilter(
    val fio: String? = null,
    val inn: String? = null,
    val birthdate: LocalDate? = null,
    val passportNumber: String? = null,

    val pagination: Pagination? = null
) {
    companion object {
        val Empty: PersonFilter = PersonFilter()
    }
}

data class Pagination(
    val page: Long = 1,
    val limit: Int = 50
)