package service.contract

import java.time.Instant

data class PersonDocument(
    val id: Long,
    val type: Type,
    val number: String,
    val createdAt: Instant
) {
    enum class Type {
        Passport,
        DriveLicense;
    }
}