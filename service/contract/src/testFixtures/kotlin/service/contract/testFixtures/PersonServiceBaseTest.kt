package service.contract.testFixtures

import io.kotest.core.spec.style.FunSpec
import io.kotest.inspectors.forAll
import io.kotest.matchers.booleans.shouldBeTrue
import io.kotest.matchers.collections.shouldContainExactlyInAnyOrder
import io.kotest.matchers.shouldBe
import service.contract.*
import java.time.LocalDate

abstract class PersonServiceBaseTest(
    builder: () -> PersonService,
) : FunSpec() {
    private val service: PersonService = builder.invoke()

    init {
        context("Person service") {
            test("create") {
                val request = PersonCreate(
                    fio = "Иванов Сидр Петрович",
                    inn = "123456789012",
                    birthdate = LocalDate.of(1998, 7, 18)
                )

                val result = service.create(request)

                with(result) {
                    fio shouldBe request.fio
                    inn shouldBe request.inn
                    birthdate shouldBe request.birthdate
                }
            }

            context("find") {
                test("get all") {
                    val createdPersons = (1..10).map { index ->
                        createPerson(index)
                    }

                    val result = service.findAll()

                    result.shouldContainExactlyInAnyOrder(createdPersons)
                }

                test("by filter") {
                    // Для массовки
                    repeat(10) { index -> createPerson(index) }

                    val fio = "Константин Федоров"
                    val birthdate = LocalDate.of(2001, 3, 19)

                    // С другой датой рождения
                    service.create(
                        PersonCreate(
                            fio = fio,
                            inn = "123456789013",
                            birthdate = birthdate.plusYears(1)
                        )
                    )

                    // с другим фио
                    service.create(
                        PersonCreate(
                            fio = "$fio Петрович",
                            inn = "123456789014",
                            birthdate = birthdate
                        )
                    )

                    val searchPersons = listOf(
                        "027656789555",
                        "027656789777",
                        "027656789999",
                    ).map { inn ->
                        val request = PersonCreate(
                            fio = fio,
                            inn = inn,
                            birthdate = birthdate
                        )

                        service.create(request)
                    }

                    val filter = PersonFilter(
                        fio = fio,
                        birthdate = birthdate
                    )

                    val result = service.findAll(filter)

                    result.shouldContainExactlyInAnyOrder(searchPersons)
                }

                test("by filter passport") {
                    // Для массовки
                    repeat(10) { index -> createPerson(index) }

                    val passport = "102030"

                    // С паспортом
                    val personWithPassport = createPerson(30)

                    val request = PersonDocumentAdd(
                        type = PersonDocument.Type.Passport,
                        number = passport
                    )

                    service.addDoc(personId = personWithPassport.id, request = request)

                    val filter = PersonFilter(
                        passportNumber = passport,
                    )

                    val result = service.findAll(filter)

                    result.single() shouldBe personWithPassport
                }
            }


            test("get by id") {
                val createdPerson = createPerson()

                val result = service.getById(createdPerson.id)

                result shouldBe createdPerson
            }

            test("get with passport") {
                val createdPerson = createPerson()

                val passportRequest = PersonDocumentAdd(
                    type = PersonDocument.Type.Passport,
                    number = "091100"
                ).also { service.addDoc(createdPerson.id, it) }

                val result = service.getPersonWithPassport(passport = passportRequest.number)

                with(result!!) {
                    fio shouldBe createdPerson.fio
                    passportNumber shouldBe passportRequest.number
                }
            }

            test("patch") {
                val createdPerson = createPerson()

                val patch = PersonPatch(
                    fio = "Новичков Имен Фамильевич",
                    birthdate = LocalDate.of(1980, 1, 28),
                    inn = null,
                )

                val result = service.patch(createdPerson.id, patch)

                result shouldBe service.getById(createdPerson.id)

                with(result) {
                    inn shouldBe createdPerson.inn

                    fio shouldBe result.fio
                    birthdate shouldBe result.birthdate
                }
            }

            context("Documents") {
                test("add") {
                    val createdPerson = createPerson()

                    val request = PersonDocumentAdd(
                        type = PersonDocument.Type.DriveLicense,
                        number = "091100"
                    )

                    val result = service.addDoc(
                        personId = createdPerson.id,
                        request = request
                    )

                    with(result) {
                        type shouldBe request.type
                        number shouldBe request.number
                    }
                }

                test("get person's docs") {
                    val createdPerson = createPerson()

                    val docs = listOf(
                        PersonDocumentAdd(
                            type = PersonDocument.Type.DriveLicense,
                            number = "091100"
                        ),
                        PersonDocumentAdd(
                            type = PersonDocument.Type.Passport,
                            number = "651101"
                        )
                    )

                    docs.forEach { request ->
                        service.addDoc(createdPerson.id, request)
                    }

                    val result = service.getDocs(createdPerson.id)

                    docs.forAll { doc ->
                        result.any { it.type == doc.type && it.number == doc.number }.shouldBeTrue()
                    }
                }
            }
        }
    }


    private suspend fun createPerson(index: Int = 1): Person {
        val request = PersonCreate(
            fio = "Иванов Сидр Петрович $index",
            inn = "123456" + "$index".padStart(6, '0'),
            birthdate = LocalDate.of(1998, 7, 18)
        )

        return service.create(request)
    }
}