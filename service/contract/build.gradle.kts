plugins {
    id("com.fix305.builder.module")
    id("java-test-fixtures")
}

dependencies {
    testImplementation("io.kotest:kotest-runner-junit5:5.9.0")
    testFixturesImplementation("io.kotest:kotest-runner-junit5:5.9.0")
}