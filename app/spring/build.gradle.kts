plugins {
    id("com.fix305.builder.module")

    id("org.springframework.boot") version "3.2.2"
    id("io.spring.dependency-management") version "1.1.4"

    kotlin("plugin.spring") version "1.9.22"
}

dependencies {
    implementation(project(":repository:contract"))
    implementation(project(":service:impl"))

    implementation("org.postgresql:postgresql:42.7.1")

    testImplementation(testFixtures(project(":service:contract")))
    testImplementation("io.kotest.extensions:kotest-extensions-spring:1.3.0")

    testImplementation(project(":flyway"))
    testImplementation("org.flywaydb:flyway-core:10.11.0")
    testImplementation("org.flywaydb:flyway-database-postgresql:10.11.0")

    testImplementation("org.testcontainers:testcontainers:1.19.8")
    testImplementation("org.testcontainers:postgresql:1.19.8")
    testImplementation("org.testcontainers:junit-jupiter:1.19.4")



    implementation("org.springframework.boot:spring-boot-starter-data-r2dbc")
    implementation("org.postgresql:r2dbc-postgresql")
    implementation("org.postgresql:postgresql")
    implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    implementation("org.jetbrains.kotlin:kotlin-reflect")

    implementation("org.springframework:spring-jdbc")

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("io.projectreactor:reactor-test")


}
repositories {
    mavenCentral()
}
