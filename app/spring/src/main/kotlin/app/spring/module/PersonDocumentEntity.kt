package app.spring.module

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.time.OffsetDateTime

@Table("person_documents")
data class PersonDocumentEntity(
    @Id
    val id: Long = 0,
    @Column("person_id")
    val personId: Long,
    val type: String,
    val number: String,
    @Column("created_at")
    val createdAt: OffsetDateTime = OffsetDateTime.now()
)