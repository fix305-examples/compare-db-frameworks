package app.spring.module

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Column
import org.springframework.data.relational.core.mapping.Table
import java.time.LocalDate
import java.time.OffsetDateTime

@Table("person")
data class PersonEntity(
    @Id
    val id: Long = 0,
    val fio: String,
    val inn: String,
    val birthdate: LocalDate,
    @Column("created_at")
    val createdAt: OffsetDateTime = OffsetDateTime.now() // null если хочешь now() из бд
)

