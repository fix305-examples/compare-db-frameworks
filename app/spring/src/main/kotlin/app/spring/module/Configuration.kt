package app.spring.module

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import repository.contract.PersonRepository
import service.contract.PersonService
import service.impl.PersonServiceImpl

@Configuration
class Configuration {
    @Bean
    fun personService(personRepository: PersonRepository): PersonService {
        return PersonServiceImpl(personRepository)
    }
}