package app.spring.module

import org.springframework.data.r2dbc.repository.Modifying
import org.springframework.data.r2dbc.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import service.contract.Person
import service.contract.PersonFilter
import service.contract.PersonPatch
import service.contract.PersonWithPassport

@Repository
interface SpringPersonDao : CoroutineCrudRepository<PersonEntity, Long> {
    @Query("SELECT p.* FROM person p LEFT JOIN person_documents d ON p.id = d.person_id WHERE " +
            "(:#{#filter.fio} IS NULL OR p.fio = :#{#filter.fio}) AND " +
            "(:#{#filter.inn} IS NULL OR p.inn = :#{#filter.inn}) AND " +
            "(:#{#filter.birthdate} IS NULL OR p.birthdate = :#{#filter.birthdate}) AND " +
            "(:#{#filter.passportNumber} IS NULL OR (d.type = 'Passport' AND d.number = :#{#filter.passportNumber}))")
    suspend fun findByComplexFilter(@Param("filter") filter: PersonFilter): List<PersonEntity>

    @Modifying
    @Query("UPDATE person SET " +
            "fio = COALESCE(:#{#patch.fio}, fio), " +
            "inn = COALESCE(:#{#patch.inn}, inn), " +
            "birthdate = COALESCE(:#{#patch.birthdate}, birthdate) " +
            "WHERE id = :id")
    @Transactional
    suspend fun patch(
        @Param("id") id: Long,
        @Param("patch") patch: PersonPatch
    )

    @Query("SELECT p.fio as fio, d.number as number " + // магия маппинга
            "FROM person p JOIN person_documents d ON d.person_id = p.id " +
            "WHERE d.type = 'Passport' AND d.number = :passport")
    suspend fun getPersonWithPassport(@Param("passport") passport: String): PersonWithPassportProjection?
}

data class PersonWithPassportProjection(
    val fio: String,
    val number: String
)


interface SpringPersonDocumentDao : CoroutineCrudRepository<PersonDocumentEntity, Long> {
    suspend fun findByPersonId(personId: Long): List<PersonDocumentEntity>
}