package app.spring.module

import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional
import repository.contract.PersonRepository
import service.contract.*

@Repository
class SpringPersonRepository(
    private val personDao: SpringPersonDao,
    private val docDao: SpringPersonDocumentDao
) : PersonRepository {
    override suspend fun create(request: PersonCreate): Person {
        val entity = PersonEntity(
            fio = request.fio,
            inn = request.inn,
            birthdate = request.birthdate,
        )

        return personDao.save(entity).toPerson()
    }

    private fun PersonEntity.toPerson(): Person {
        return Person(
            id = id,
            fio = fio,
            inn = inn,
            birthdate = birthdate,
            createdAt = createdAt.toInstant()
        )
    }

    override suspend fun getById(id: Long): Person? {
        return personDao.findById(id)?.toPerson()
    }

    override suspend fun findAll(filter: PersonFilter): List<Person> {
        return personDao.findByComplexFilter(filter).map { it.toPerson() }
    }

    @Transactional
    override suspend fun patch(id: Long, request: PersonPatch): Person {
        personDao.patch(id, request)

        return personDao.findById(id)?.toPerson()
            ?: error("shouldn't be null")
    }

    override suspend fun addDoc(personId: Long, request: PersonDocumentAdd): PersonDocument {
        val docEntity = PersonDocumentEntity(
            personId = personId,
            type = request.type.name,
            number = request.number,
        )

        return docDao.save(docEntity)
            .toPersonDocument()
    }

    private fun PersonDocumentEntity.toPersonDocument(): PersonDocument {
        return PersonDocument(
            id = id,
            type = PersonDocument.Type.entries.single { it.name == type },
            number = number,
            createdAt = createdAt.toInstant()
        )
    }

    override suspend fun getDocs(personId: Long): List<PersonDocument> {
        return docDao.findByPersonId(personId)
            .map { it.toPersonDocument() }
            .toList()
    }

    override suspend fun getPersonWithPassport(passport: String): PersonWithPassport? {
        return personDao.getPersonWithPassport(passport)?.let {
            PersonWithPassport(fio = it.fio, passportNumber = it.number)
        }
    }

    override suspend fun clean() {
        docDao.deleteAll()
        personDao.deleteAll()
    }
}