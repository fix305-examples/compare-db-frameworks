package app.spring

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DbFrameworkApp

fun main(args: Array<String>) {
    runApplication<DbFrameworkApp>(*args)
}