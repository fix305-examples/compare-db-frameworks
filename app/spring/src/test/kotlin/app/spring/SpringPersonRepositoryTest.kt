package app.spring

import io.kotest.core.extensions.Extension
import io.kotest.core.test.TestCase
import io.kotest.extensions.spring.SpringExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.containers.PostgreSQLContainer
import repository.contract.PersonRepository
import service.contract.testFixtures.PersonServiceBaseTest
import service.impl.PersonServiceImpl


@SpringBootTest
@ContextConfiguration(classes = [DbFrameworkApp::class])
class SpringPersonRepositoryTest(
    @Autowired
    private val repository: PersonRepository,
) : PersonServiceBaseTest({ PersonServiceImpl(repository) }) {
    override fun extensions(): List<Extension> = listOf(SpringExtension)

    override suspend fun beforeEach(testCase: TestCase) {
        repository.clean()
    }

    companion object {
        private val pgContainer by lazy {
            PostgreSQLContainer<Nothing>("postgres:14.2")
                .apply { start() }
        }

        @JvmStatic
        @DynamicPropertySource
        fun properties(registry: DynamicPropertyRegistry) {
            registry.add("spring.r2dbc.url") { r2dbcUrl() }
            registry.add("spring.r2dbc.password") { pgContainer.password }
            registry.add("spring.r2dbc.username") { pgContainer.username }

            registry.add("spring.flyway.enabled") { true }
            registry.add("spring.flyway.url") { pgContainer.jdbcUrl }
            registry.add("spring.flyway.user") { pgContainer.username }
            registry.add("spring.flyway.password") { pgContainer.password }

            registry.add("logging.level.org.springframework.data.r2dbc") { "DEBUG" }
            registry.add("logging.level.io.r2dbc.postgresql.QUERY") { "DEBUG" }

            registry.add("spring.flyway.locations") { "classpath:db/migration" }
        }

        private fun r2dbcUrl(): String {
            return "r2dbc:postgresql://${pgContainer.host}:${
                pgContainer.getMappedPort(
                    PostgreSQLContainer.POSTGRESQL_PORT
                )
            }/${pgContainer.databaseName}"
        }
    }
}